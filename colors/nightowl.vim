set background=dark
set termguicolors
hi clear
syntax reset
let g:colors_name="nightowl"

" {{{

" TODO Option to set background
hi Normal                   guifg=#d6deeb guibg=NONE    gui=NONE

hi Visual                   guifg=NONE    guibg=#1d3b53 gui=NONE
hi NonText                  guifg=#253745 guibg=NONE    gui=NONE "editorWhitespace.foreground": "#e3e4e229"
hi LineNr                   guifg=#4b6479 guibg=NONE    gui=NONE "editorLineNumber.foreground"
hi Cursor                   guifg=#80a4c2 guibg=NONE    gui=NONE "editorCursor.foreground"
hi TermCursor               guifg=#234d70 guibg=#cccccc gui=NONE "terminalCursor.background", "terminalCursor.foreground": null

" REVIEW TermCursorNC should be an empty box

" TODO Separate options for current line, gutter, signcolumn, and foldcolumn.
hi CursorLine               guifg=NONE    guibg=#01121f gui=NONE "editor.lineHighlightBackground": "#00000033"
hi CursorColumn             guifg=NONE    guibg=#01121f gui=NONE
hi CursorLineNr             guifg=#c5e4fd guibg=NONE    gui=NONE "editorLineNumber.activeForeground"

" REVIEW Couldn't find any reference but Digital Color Meter says the 3 dots
" when text is folded is rgb(128, 128, 128).
hi Conceal                  guifg=#808080 guibg=NONE    gui=NONE

" TODO Option for highlighting folded line(s)
" TODO Keep coloring of first line and use that as foldtext
hi Folded                   guifg=NONE    guibg=#092134 gui=NONE "editor.foldBackground": "#1d3b534d"

hi FoldColumn               guifg=#c5c5c5 guibg=#011627 gui=NONE "editorGutter.foldingControlForeground"
hi SignColumn               guifg=NONE    guibg=#011627 gui=NONE
hi WinSeparator             guifg=#011627 guibg=#011627 gui=NONE

" REVIEW Original theme has borders; 2 colors for "underline" and "separators"
hi TabLine                  guifg=#5f7e97 guibg=#01111d gui=NONE "tab.inactiveBackground", "tab.inactiveForeground"
hi TabLineFill              guifg=NONE    guibg=#011627 gui=NONE
hi TabLineSel               guifg=#d2dee7 guibg=#0b2942 gui=NONE "tab.activeBackground", "tab.activeForeground"

" TODO VSCode has no statusline. Instead it uses the tablines as window
" separators. There is no winning here. If using some statusline plugin like
" lualine.nvim, then inactive tabs' background plus custom foreground for each
" "element" might be OK. Ideally I'd want to replicate my Emacs setup, with
" one tabline each window, and a single line as statusline + minibuffer + echo
" area in the very bottom.
hi StatusLine               guifg=NONE    guibg=NONE    gui=reverse,bold
hi StatusLineNC             guifg=NONE    guibg=NONE    gui=reverse,bold

" REVIEW VSCode breadcrumbs changes foreground to "#ffffff" on hover.
hi WinBar                   guifg=#a599e9 guibg=NONE    gui=NONE "breadcrumb.foreground"

hi Pmenu                    guifg=#d6deeb guibg=#2c3043 gui=NONE "editorSuggestWidget.background", "editorSuggestWidget.foreground"
hi PmenuSel                 guifg=#ffffff guibg=#5f7e97 gui=NONE "editorSuggestWidget.highlightForeground", editorSuggestWidget.selectedBackground"
hi PmenuThumb               guifg=NONE    guibg=#053254 gui=NONE "scrollbarSlider.background": "#084d8180"
hi WildMenu                 guifg=#ffffff guibg=#5f7e97 gui=NONE

" TODO When searching, original theme also changes current line highlight to
" "editor.hoverHighlightBackground": "#7e57c25a".
hi Search                   guifg=NONE    guibg=#063f5d gui=NONE "editor.findMatchHighlightBackground": "#1085bb5d"
hi IncSearch                guifg=NONE    guibg=#4a4d80 gui=NONE "editor.findMatchBackground": "#5f7e9779" and "editor.hoverHighlightBackground": "#7e57c25a"
hi CurSearch                guifg=NONE    guibg=#4a4d80 gui=NONE
hi QuickFixLine             guifg=NONE    guibg=#2d2d5e gui=NONE "editor.hoverHighlightBackground": "#7e57c25a"

" REVIEW This *should* be a thin vertical line.
hi ColorColumn              guifg=NONE    guibg=#1f385d gui=NONE "editorRuler.foreground": "#5e81ce52"

hi clear Directory

" REVIEW VSCode's diff editor shows "changed" lines as deleted (red) on the
" left and inserted (green) on right. Completely removed lines are shown as
" diagnonal fill "#cccccc33". Changed text also has a border that is slightly
" more opaque than the background.
"
" Background on entire line:
" - "diffEditor.insertedLineBackground": "#9bb95533"
" - "diffEditor.removedLineBackground": "#ff000033"
"
" Background of changed text (on top of the line background):
" - "diffEditor.insertedTextBackground": "#99b76d23"
" - "diffEditor.insertedTextBorder": "#c5e47833"
" - "diffEditor.removedTextBackground": "#ef535033"
" - "diffEditor.removedTextBorder": "#ef53504d"
"
" Let's just add 20% alpha to the git gutter colors instead. (Blend against
" the default background #011627.)
" - "editorGutter.addedBackground": "#9ccc65"
" - "editorGutter.deletedBackground": "#ef5350"
" - "editorGutter.modifiedBackground": "#e2b93d"
hi DiffAdd                  guifg=NONE    guibg=#203a33 gui=NONE
hi DiffChange               guifg=NONE    guibg=NONE    gui=NONE
hi DiffDelete               guifg=NONE    guibg=#31222f gui=NONE " REVIEW Really should be guibg=NONE plus a gray-ish diagonal fill
hi DiffText                 guifg=NONE    guibg=#2e372b gui=NONE

" TODO How to inherit part of the style?
" TODO "Notifications" here refers to the popup banners in VSCode, as far as I
" can tell. I probably shouldn't mix foreground meant for one UI element with
" background of another.
"
" - "notificationCenter.border": "#262a39"
" - "notificationToast.border": "#262a39"
" - "notifications.border": "#262a39"
" - "notificationLink.foreground": "#80cbc4"
" - "notifications.background": "#01111d"
" - "notifications.foreground": "#ffffffcc"
"
" - "notificationsErrorIcon.foreground": "#ef5350"
" - "notificationsWarningIcon.foreground": "#b39554"
" - "notificationsInfoIcon.foreground": "#3794ff"
hi clear MsgArea
hi clear MsgSeparator
hi ModeMsg                  guifg=NONE    guibg=NONE    gui=bold
hi ErrorMsg                 guifg=#ef5350 guibg=NONE    gui=NONE
hi WarningMsg               guifg=#b39554 guibg=NONE    gui=NONE
hi MoreMsg                  guifg=#3794ff guibg=NONE    gui=NONE
hi Question                 guifg=#80cbc4 guibg=NONE    gui=NONE

" REVIEW No idea if VSCode as anthing related to this.
hi Title                    guifg=#82B1FF guibg=NONE    gui=NONE "markup.heading.markdown"

" TODO Should have a border/box around matching paren with. Current background
" alone is not really enough contrast.
hi MatchParen               guifg=NONE    guibg=#1d3549 gui=underline guisp=#888888 "editorBracketMatch.background": "#5f7e974d"

" TODO Couldn't find any reference; hard-coded as far as I know.
hi SpecialKey               guifg=#ffffff guibg=#960000 gui=NONE

" ... I'm just making stuff up at this point.
" TODO Make sure these does not conflict with other highlighting, e.g.
" underlined links in text files.
hi SpellBad                 guifg=NONE    guibg=NONE    gui=undercurl guisp=#F44747 "token.error-token"
hi SpellCap                 guifg=NONE    guibg=NONE    gui=undercurl guisp=#B267E6 "token.debug-token"
hi SpellRare                guifg=NONE    guibg=NONE    gui=undercurl guisp=#CD9731 "token.warn-token"
hi SpellLocal               guifg=NONE    guibg=NONE    gui=undercurl guisp=#6796E6 "token.info-token"

hi DiagnosticError          guifg=#ef5350 guibg=NONE    gui=NONE  "editorError.foreground": "#ef5350"
hi DiagnosticWarn           guifg=#b39554 guibg=NONE    gui=NONE  "editorWarning.foreground": "#b39554"
hi DiagnosticInfo           guifg=#3794ff guibg=NONE    gui=NONE  "editorInfo.foreground": "#3794ff"
hi DiagnosticHint           guifg=#a7aeb3 guibg=NONE    gui=NONE  "editorHint.foreground": "#eeeeeeb3"
hi DiagnosticUnderlineError guifg=NONE    guibg=NONE    gui=undercurl guisp=#ef5350
hi DiagnosticUnderlineWarn  guifg=NONE    guibg=NONE    gui=undercurl guisp=#b39554
hi DiagnosticUnderlineInfo  guifg=NONE    guibg=NONE    gui=undercurl guisp=#3794ff
hi DiagnosticUnderlineHint  guifg=NONE    guibg=NONE    gui=undercurl guisp=#a7aeb3

" }}}
" {{{

hi Comment                  guifg=#637777 guibg=NONE    gui=italic "comment"

" REVIEW I'm not sure how SpecialComment is used in Vim. The color below is
" used for doxygen tags in comments (like @param).
hi SpecialComment           guifg=#C792EA guibg=NONE    gui=NONE "storage.type"

" Plain variables need no special highlighting.
hi Identifier               guifg=NONE    guibg=NONE    gui=NONE

" "#82AAFF", "#C792EA", and other colors are used in original theme for functions/methods.
hi Function                 guifg=#82AAFF guibg=NONE    gui=italic "entity.name.function"

hi Type                     guifg=#C792EA guibg=NONE    gui=NONE
hi StorageClass             guifg=#C792EA guibg=NONE    gui=NONE
hi Structure                guifg=#C792EA guibg=NONE    gui=NONE
hi Typedef                  guifg=#C792EA guibg=NONE    gui=NONE

" For, while; If, switch; import, from, and many more. However, there are
" exception in the original theme (no italic, different color).
" I want italic here mostly to make them different from other "entity names",
" since this purple color is used quite often.
hi Conditional              guifg=#C792EA guibg=NONE    gui=italic "keyword.control"
hi Exception                guifg=#C792EA guibg=NONE    gui=italic
hi Repeat                   guifg=#C792EA guibg=NONE    gui=italic
hi Label                    guifg=#c792ea guibg=NONE    gui=NONE
hi Keyword                  guifg=#C792EA guibg=NONE    gui=NONE
hi Statement                guifg=#C792EA guibg=NONE    gui=italic " fallback...

" - Preprocessor directives (#ifdef, #define, ...) use this color. The ProProc
"   group alones would have been enough but *some* filetypes just decides to
"   classify the entire macro to this group, generating a purple wall of text.
" - Names of macros should be: #7FDBCA.
hi Include                  guifg=#C792EA guibg=NONE    gui=NONE
hi Define                   guifg=#C792EA guibg=NONE    gui=NONE
hi Macro                    guifg=#C792EA guibg=NONE    gui=NONE
hi PreCondit                guifg=#C792EA guibg=NONE    gui=NONE
hi PreProc                  guifg=NONE    guibg=NONE    gui=NONE " fallback...

" Different colors are used. See various "*punctuation*" tokens.
hi Delimiter                guifg=NONE    guibg=NONE    gui=NONE

" Deliberately different from original theme. Should only use color to help
" distinguish operators from things around them. For reference:
"   - Assignment, arithmetic, bitwise, ternary: "#C792EA"
"   - "keyword.operator.relational": "#C792EA" *and* italic
"   - "keyword.operator": "#7FDBCA"
hi Operator                 guifg=#C792EA guibg=NONE    gui=italic

" Color of string *excluding* the surrounding quotes.
hi String                   guifg=#ECC48D guibg=NONE    gui=NONE "string"

" This is deliberately different from original theme, which uses the same
" color for C characters ('A') and C strings ("A").
" Also, I want escaped characters ('\0') to have same color as normal
" characters.
" See "constant.character.numeric" and "constant.character.escape".
hi Character                guifg=#82AAFF guibg=NONE    gui=NONE "constant.character"

" Use this for escaped characters *inside strings*. Don't use this for
" "normal" characters, like '\0'.
" Also, use "#5f7e97" (input.placeholderForeground) for placeholders, like
" "%s" in C or "{}" in Python.
hi SpecialChar              guifg=#F78C6C guibg=NONE    gui=NONE "constant.character.escape"

hi Number                   guifg=#F78C6C guibg=NONE    gui=NONE "constant.numeric"

" For Boolean, and null-ish values if applicable.
hi Boolean                  guifg=#FF5874 guibg=NONE    gui=NONE "constant.language.boolean"

" For constants that is not Boolean, numbers, characters or strings. (Note:
" quite a few filetypes just use this class for Booleans... need to fix.)
hi Constant                 guifg=#82AAFF guibg=NONE    gui=NONE "constant.language", "punctuation.definition.constant", "variable.other.constant"

" Note: this color is not for diagnostic errors but for "tokens" that means
" "error", e.g. "fail" in a compilation log.
hi Error                    guifg=#F44747 guibg=NONE    gui=NONE "token.error-token"

hi Todo                     guifg=#C792EA guibg=NONE    gui=NONE

" REVIEW Couldn't find anything similar in VSCode. Vim's help itself says
" consider using the conceal mechanism here... might as well.
hi clear Ignore
hi link Ignore Conceal

hi Underlined               guifg=#ff869a guibg=NONE    gui=underline "markup.underline.link.markdown"

" Like "debugger;" in JavaScript. (And of course Vim doesn't use this group for js.)
hi Debug                    guifg=#7FDBCA guibg=NONE    gui=NONE "keyword.other.debugger"

" REVIEW Not clear what this is for.
hi Tag                      guifg=#80cbc4 guibg=NONE    gui=NONE "notificationLink.foreground"

" Vim (ab)uses this class for many different things. But this color should
" only be used sparingly, when the symbol is actually *special*.
hi Special                  guifg=#8EACE3 guibg=NONE    gui=NONE "variable.language.special"

" }}}
